<?php
/**
 * Plugin Name:     Core Functionality
 * Plugin URI:
 * Description:     Custom core functionality for tomdispatch.com
 * Author:          Misfist
 * Author URI:      https://patricia-lutz.com
 * Text Domain:     tomdispatch-core
 * Domain Path:     /languages
 * Version:         0.0.1
 *
 * @package         Tomdispatch_Core
 */

 if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Plugin Directory
 *
 * @since 0.0.1
 */
define( 'SITE_CORE_DIR', dirname( __FILE__ ) );

require_once( 'includes/security.php' );
require_once( 'includes/performance.php' );
require_once( 'includes/seo.php' );

require_once( 'includes/helpers.php' );
require_once( 'includes/custom-post-types.php' );
require_once( 'includes/custom-fields.php' );
require_once( 'includes/admin.php' );
require_once( 'includes/public.php' );
