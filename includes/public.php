<?php
/**
 * Core Public Functions
 *
 * @package    Tomdispatch_Core
 * @subpackage Tomdispatch_Core\Includes
 * @since      0.0.1
 * @license    GPL-2.0+
 */

add_filter( 'pre_option_link_manager_enabled', '__return_true' );
