<?php
/**
 * Custom Fields Functions
 *
 * @package    Tomdispatch_Core
 * @subpackage Tomdispatch_Core\Includes
 * @since      0.0.1
 * @license    GPL-2.0+
 */

/**
 * Register Custom Post Publisher
 *
 * @since 0.0.1
 *
 * @return void
 */
function tomdispatch_core_register_custom_post() {

	$labels = array(
		'name'                  => _x( 'Books', 'Post Publisher General Name', 'tomdispatch-core' ),
		'singular_name'         => _x( 'Book', 'Post Publisher Singular Name', 'tomdispatch-core' ),
		'menu_name'             => __( 'Books', 'tomdispatch-core' ),
		'name_admin_bar'        => __( 'Book', 'tomdispatch-core' ),
		'archives'              => __( 'Book Archives', 'tomdispatch-core' ),
		'attributes'            => __( 'Book Attributes', 'tomdispatch-core' ),
		'parent_item_colon'     => __( 'Parent Book:', 'tomdispatch-core' ),
		'all_items'             => __( 'All Books', 'tomdispatch-core' ),
		'add_new_item'          => __( 'Add New Book', 'tomdispatch-core' ),
		'add_new'               => __( 'Add New', 'tomdispatch-core' ),
		'new_item'              => __( 'New Book', 'tomdispatch-core' ),
		'edit_item'             => __( 'Edit Book', 'tomdispatch-core' ),
		'update_item'           => __( 'Update Book', 'tomdispatch-core' ),
		'view_item'             => __( 'View Book', 'tomdispatch-core' ),
		'view_items'            => __( 'View Books', 'tomdispatch-core' ),
		'search_items'          => __( 'Search Book', 'tomdispatch-core' ),
		'not_found'             => __( 'Not found', 'tomdispatch-core' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'tomdispatch-core' ),
		'featured_image'        => __( 'Book Cover', 'tomdispatch-core' ),
		'set_featured_image'    => __( 'Set book cover', 'tomdispatch-core' ),
		'remove_featured_image' => __( 'Remove book cover', 'tomdispatch-core' ),
		'use_featured_image'    => __( 'Use as book cover', 'tomdispatch-core' ),
		'insert_into_item'      => __( 'Insert into book', 'tomdispatch-core' ),
		'uploaded_to_this_item' => __( 'Uploaded to this book', 'tomdispatch-core' ),
		'items_list'            => __( 'Books list', 'tomdispatch-core' ),
		'items_list_navigation' => __( 'Books list navigation', 'tomdispatch-core' ),
		'filter_items_list'     => __( 'Filter books list', 'tomdispatch-core' ),
	);
	$args = array(
		'label'                 => __( 'Book', 'tomdispatch-core' ),
		'description'           => __( 'Book Post Publisher', 'tomdispatch-core' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
		'taxonomies'            => array( 'book_type' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-book-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'book', $args );

}
add_action( 'init', 'tomdispatch_core_register_custom_post', 0 );

/**
 * Register Custom Taxonomy
 *
 * @since 0.0.1
 *
 * @return void
 */
function tomdispatch_core_custom_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Publishers', 'Taxonomy General Name', 'tomdispatch-core' ),
		'singular_name'              => _x( 'Publisher', 'Taxonomy Singular Name', 'tomdispatch-core' ),
		'menu_name'                  => __( 'Publisher', 'tomdispatch-core' ),
		'all_items'                  => __( 'All Publishers', 'tomdispatch-core' ),
		'parent_item'                => __( 'Parent Publisher', 'tomdispatch-core' ),
		'parent_item_colon'          => __( 'Parent Publisher:', 'tomdispatch-core' ),
		'new_item_name'              => __( 'New Publisher Name', 'tomdispatch-core' ),
		'add_new_item'               => __( 'Add New Publisher', 'tomdispatch-core' ),
		'edit_item'                  => __( 'Edit Publisher', 'tomdispatch-core' ),
		'update_item'                => __( 'Update Publisher', 'tomdispatch-core' ),
		'view_item'                  => __( 'View Publisher', 'tomdispatch-core' ),
		'separate_items_with_commas' => __( 'Separate publishers with commas', 'tomdispatch-core' ),
		'add_or_remove_items'        => __( 'Add or remove publishers', 'tomdispatch-core' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'tomdispatch-core' ),
		'popular_items'              => __( 'Popular Publishers', 'tomdispatch-core' ),
		'search_items'               => __( 'Search Publishers', 'tomdispatch-core' ),
		'not_found'                  => __( 'Not Found', 'tomdispatch-core' ),
		'no_terms'                   => __( 'No publishers', 'tomdispatch-core' ),
		'items_list'                 => __( 'Publishers list', 'tomdispatch-core' ),
		'items_list_navigation'      => __( 'Publishers list navigation', 'tomdispatch-core' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true,
	);
	register_taxonomy( 'book_type', array( 'book' ), $args );

}
add_action( 'init', 'tomdispatch_core_custom_taxonomy', 0 );
