<?php
/**
 * Custom Fields Functions
 *
 * @package    Tomdispatch_Core
 * @subpackage Tomdispatch_Core\Includes
 * @since      0.0.1
 * @license    GPL-2.0+
 */


 if( function_exists( 'acf_add_local_field_group' ) ) {
   acf_add_local_field_group( array(
     'key' => 'group_purchase_information',
     'title' => __( 'Purchase Information', 'tomdispatch' ),
     'fields' => array (
       array (
         'key' => 'field_title',
         'label' => __( 'Title', 'tomdispatch' ),
         'name' => 'purchase_title',
         'type' => 'text',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array (
           'width' => '',
           'class' => '',
           'id' => '',
         ),
         'default_value' => '',
         'placeholder' => __( 'Book Title', 'tomdispatch' ),
         'prepend' => '',
         'append' => '',
         'maxlength' => '',
       ),
       array (
         'key' => 'field_purchase_image',
         'label' => __( 'Image', 'tomdispatch' ),
         'name' => 'purchase_image',
         'type' => 'image',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array (
           'width' => '',
           'class' => '',
           'id' => '',
         ),
         'return_format' => 'array',
         'preview_size' => 'thumbnail',
         'library' => 'all',
         'min_width' => '',
         'min_height' => '',
         'min_size' => '',
         'max_width' => '',
         'max_height' => '',
         'max_size' => '',
         'mime_types' => '',
       ),
       array (
         'key' => 'field_purchase_link',
         'label' => __( 'Link', 'tomdispatch' ),
         'name' => 'purchase_link',
         'type' => 'url',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array (
           'width' => '',
           'class' => '',
           'id' => '',
         ),
         'default_value' => '',
         'placeholder' => '',
       ),
       array (
         'key' => 'field_link_text',
         'label' => __( 'Link Text', 'tomdispatch' ),
         'name' => 'purchase_link_text',
         'type' => 'text',
         'instructions' => '',
         'required' => 0,
         'conditional_logic' => 0,
         'wrapper' => array (
           'width' => '',
           'class' => '',
           'id' => '',
         ),
         'default_value' => 'Buy the Book',
         'placeholder' => '',
         'prepend' => '',
         'append' => '',
         'maxlength' => '',
       ),
     ),
     'location' => array (
       array (
         array (
           'param' => 'post_type',
           'operator' => '==',
           'value' => 'post',
         ),
       ),
       array (
         array (
           'param' => 'post_type',
           'operator' => '==',
           'value' => 'book',
         ),
       ),
     ),
     'menu_order' => 0,
     'position' => 'side',
     'style' => 'default',
     'label_placement' => 'top',
     'instruction_placement' => 'label',
     'hide_on_screen' => '',
     'active' => 1,
     'description' => '',
   ));

 }
