<?php
/**
 * Core Performance Functions
 *
 * @package    Tomdispatch_Core
 * @subpackage Tomdispatch_Core\Includes
 * @since      0.0.1
 * @license    GPL-2.0+
 */

/**
 * Remove query strings from static resources
 *
 * @since   0.0.1
 *
 * @uses    script_loader_src filter
 * @uses    style_loader_src filter
 *
 * @param   {string} $src
 * @return  {string} $parts[0]
 */
function core_functions_remove_script_version( $src ){
   $parts = explode( '?ver', $src );
   return $parts[0];
}
add_filter( 'script_loader_src', 'core_functions_remove_script_version', 15, 1 );
add_filter( 'style_loader_src', 'core_functions_remove_script_version', 15, 1 );
